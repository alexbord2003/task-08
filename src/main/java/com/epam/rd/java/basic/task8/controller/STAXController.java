package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.constants.*;
import com.epam.rd.java.basic.task8.entities.*;
import com.epam.rd.java.basic.task8.entities.growing.*;
import com.epam.rd.java.basic.task8.entities.parameters.*;

import org.xml.sax.helpers.DefaultHandler;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.*;
import javax.xml.transform.stream.StreamSource;

public class STAXController extends DefaultHandler {
	private final String xmlFileName;
	private Flowers flowers;
	private Flower flower;
	private VisualParameters visualParameters;
	private GrowingTips growingTips;
	private AveLenFlower aveLenFlower;
	private Temperature temperature;
	private Lighting lighting;
	private Watering watering;
	private String currentElement;

	public STAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public Flowers getFlowers() {
		return flowers;
	}

	public void parse() throws XMLStreamException {
		XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
		xmlInputFactory.setProperty(XMLInputFactory.IS_NAMESPACE_AWARE, true);
		xmlInputFactory.setProperty(XMLInputFactory.SUPPORT_DTD, false);
		XMLEventReader xmlEventReader = xmlInputFactory.createXMLEventReader(new StreamSource(xmlFileName));
		while (xmlEventReader.hasNext()) {
			XMLEvent xmlEvent = xmlEventReader.nextEvent();
			if (xmlEvent.isCharacters() && xmlEvent.asCharacters().isWhiteSpace()) {
				xmlEvent = xmlEventReader.nextEvent();
			}
			if (xmlEvent.isStartElement()) {
				processStartElement(xmlEvent);
			}
			if (xmlEvent.isCharacters()) {
				characterProcessing(flower, currentElement, xmlEvent);
			}
			if (xmlEvent.isEndElement()) {
				processEndElement(flower, xmlEvent);
			}
		}
		xmlEventReader.close();
	}

	private void processStartElement(XMLEvent xmlEvent) {
		StartElement startElement = xmlEvent.asStartElement();
		currentElement = startElement.getName().getLocalPart();
		if (XML.FLOWERS.equalsTo(currentElement)){
			flowers = new Flowers();
			return;
		}
		if (XML.FLOWER.value().equals(currentElement)) {
			flower = new Flower();
			return;
		}
		if (XML.VISUAL_PARAMETERS.value().equals(currentElement)) {
			visualParameters = new VisualParameters();
			return;
		}
		if (XML.AVE_LEN_FLOWER.value().equals(currentElement)) {
			aveLenFlower = new AveLenFlower();
			Attribute attribute = startElement
					.getAttributeByName(QName.valueOf(XML.MEASURE.value()));
			if (attribute != null) {
				aveLenFlower.setMeasure(attribute.getValue());
			}
			return;
		}
		if (XML.GROWING_TIPS.value().equals(currentElement)) {
			growingTips = new GrowingTips();
			return;
		}
		if (XML.TEMPERATURE.value().equals(currentElement)) {
			temperature = new Temperature();
			Attribute attribute = startElement
					.getAttributeByName(QName.valueOf(XML.MEASURE.value()));
			if (attribute != null) {
				temperature.setMeasure(attribute.getValue());
			}
			return;
		}
		if (XML.LIGHTING.value().equals(currentElement)) {
			lighting = new Lighting();
			Attribute attribute = startElement
					.getAttributeByName(QName.valueOf(XML.LIGHT_REQUIRING.value()));
			if (attribute != null) {
				lighting.setLightRequiring(attribute.getValue());
			}
			return;
		}
		if (XML.WATERING.value().equals(currentElement)) {
			watering = new Watering();
			Attribute attribute = startElement
					.getAttributeByName(QName.valueOf(XML.MEASURE.value()));
			if (attribute != null) {
				watering.setMeasure(attribute.getValue());
			}
		}
	}

	private void processEndElement(Flower flower, XMLEvent xmlEvent) {
		EndElement endElement = xmlEvent.asEndElement();
		String localName = endElement.getName().getLocalPart();
		if (XML.FLOWER.value().equals(localName)) {
			flowers.getFlower().add(flower);
			return;
		}
		if (XML.VISUAL_PARAMETERS.value().equals(localName)) {
			flower.getVisualParameters().add(visualParameters);
			return;
		}
		if (XML.GROWING_TIPS.value().equals(localName)) {
			flower.getGrowingTips().add(growingTips);
			return;
		}
		if (XML.AVE_LEN_FLOWER.value().equals(localName)) {
			visualParameters.getAveLenFlower().add(aveLenFlower);
			return;
		}
		if (XML.TEMPERATURE.value().equals(localName)) {
			growingTips.getTemperature().add(temperature);
			return;
		}
		if (XML.LIGHTING.value().equals(localName)) {
			growingTips.getLighting().add(lighting);
			return;
		}
		if (XML.WATERING.value().equals(localName)) {
			growingTips.getWatering().add(watering);
		}
	}

	private void characterProcessing(Flower flower, String currentElement, XMLEvent xmlEvent) {
		Characters characters = xmlEvent.asCharacters();
		if (XML.FLOWER_NAME.value().equals(currentElement)) {
			flower.setName(characters.getData());
			return;
		}
		if (XML.FLOWER_SOIL.value().equals(currentElement)) {
			flower.setSoil(characters.getData());
			return;
		}
		if (XML.FLOWER_ORIGIN.value().equals(currentElement)) {
			flower.setOrigin(characters.getData());
			return;
		}
		if (XML.FLOWER_MULTIPLYING.equalsTo(currentElement)) {
			flower.setMultiplying(characters.getData());
			return;
		}
		if (XML.STEM_COLOUR.value().equals(currentElement)) {
			visualParameters.setStemColour(characters.getData());
			return;
		}
		if (XML.LEAF_COLOUR.value().equals(currentElement)) {
			visualParameters.setLeafColour(characters.getData());
			return;
		}
		if (XML.AVE_LEN_FLOWER.equalsTo(currentElement)){
			aveLenFlower.setContent(Integer.parseInt(characters.getData()));
			return;
		}
		if (XML.TEMPERATURE.equalsTo(currentElement)){
			temperature.setContent(Integer.parseInt(characters.getData()));
			return;
		}
		if (XML.WATERING.equalsTo(currentElement)){
			watering.setContent(Integer.parseInt(characters.getData()));
		}
	}
}
