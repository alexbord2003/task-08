package com.epam.rd.java.basic.task8.util;


import java.util.Comparator;
import com.epam.rd.java.basic.task8.entities.*;

public class Sorter {
    private Sorter() {}

    public static final Comparator<Flower> COMPARE_BY_FLOWER_NAME
            = (Comparator.comparing(Flower::getName));

    public static final Comparator<Flower> COMPARE_BY_FLOWER_SOIL
            = (Comparator.comparing(Flower::getSoil));

    public static final Comparator<Flower> COMPARE_BY_FLOWER_ORIGIN
            = (Comparator.comparing(Flower::getOrigin));

    public static void sortFlowerName(Flowers flowers) {
        flowers.getFlower().sort(COMPARE_BY_FLOWER_NAME);
    }

    public static void sortFlowerSoil(Flowers flowers) {
        flowers.getFlower().sort(COMPARE_BY_FLOWER_SOIL);
    }

    public static void sortFlowerOrigin(Flowers flowers) {
        flowers.getFlower().sort(COMPARE_BY_FLOWER_ORIGIN);
    }
}
