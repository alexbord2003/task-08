package com.epam.rd.java.basic.task8.entities;

import com.epam.rd.java.basic.task8.entities.growing.GrowingTips;
import com.epam.rd.java.basic.task8.entities.parameters.VisualParameters;

import java.util.ArrayList;
import java.util.List;

public class Flower {
    private String name;
    private String soil;
    private String origin;
    private String multiplying;
    private List<VisualParameters> visualParameters;
    private List<GrowingTips> growingTips;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSoil() {
        return soil;
    }

    public void setSoil(String soil) {
        this.soil = soil;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getMultiplying() {
        return multiplying;
    }

    public void setMultiplying(String multiplying) {
        this.multiplying = multiplying;
    }

    public List<VisualParameters> getVisualParameters() {
        if (visualParameters == null) {
            visualParameters = new ArrayList<>();
        }
        return visualParameters;
    }

    public List<GrowingTips> getGrowingTips() {
        if (growingTips == null) {
            growingTips = new ArrayList<>();
        }
        return growingTips;
    }

    @Override
    public String toString() {
        return "Flower{" +
                "name='" + name + '\'' +
                ", soil='" + soil + '\'' +
                ", origin='" + origin + '\'' +
                ", parameters=" + visualParameters +
                ", growing=" + growingTips +
                ", multiplying='" + multiplying + '\'' +
                '}';
    }
}
