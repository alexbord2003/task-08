package com.epam.rd.java.basic.task8.entities.growing;

import java.util.ArrayList;
import java.util.List;

public class GrowingTips {
    private List<Temperature> temperature;
    private List<Lighting> lighting;
    private List<Watering> watering;

    public List<Temperature> getTemperature() {
        if (temperature == null){
            temperature = new ArrayList<>();
        }
        return temperature;
    }

    public List<Lighting> getLighting() {
        if (lighting == null){
            lighting = new ArrayList<>();
        }
        return lighting;
    }

    public List<Watering> getWatering() {
        if (watering == null){
            watering = new ArrayList<>();
        }
        return watering;
    }

    @Override
    public String toString() {
        return "{" +
                "temperature=" + temperature +
                ", lighting=" + lighting +
                ", watering=" + watering +
                '}';
    }
}
